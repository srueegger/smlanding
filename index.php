<?PHP
if($_SERVER["HTTPS"] != "on") {
   header("HTTP/1.1 301 Moved Permanently");
   header("Location: https://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]);
   exit();
}
?>
<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="de">
    <!--<![endif]-->
    <head>
        <!-- Basic Info -->
        <meta charset="utf-8">
        <title>Social Meal - Meal-Sharing für die Region Basel</title>
        <meta name="description" content="Privates kochen in Basel. Wir vernetzen Menschen über das Essen.">
        <meta name="author" content="Social Meal">
        <!-- Mobile  -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- Font -->
        <link href='https://fonts.googleapis.com/css?family=Roboto:500,500italic,100,100italic,300,300italic' rel='stylesheet' type='text/css'>
        <!-- CSS -->
        <link rel="stylesheet" href="font-awesome/css/font-awesome.css" />
        <link rel="stylesheet" href="css/style.css" />
        <!--[if lt IE 9]><script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->
        <!-- JS -->      
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(["setDocumentTitle", document.domain + "/" + document.title]);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//stats.2lounge.ch/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 9]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img src="//stats.2lounge.ch/piwik.php?idsite=9" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->

    </head>

    <body>
        <div id="modal-window">
            <div class="container">
                <!-- Header -->
                <header id="header"> 
                    <div id="logo"> <a href="https://socialmeal.ch">Social Meal</a></div>
                </header>
                <!-- Modal Open -->
                <a  id="modal-open"><i class="fa fa-plus"> Mehr erfahren ...</i></a>

                <!-- Main Title -->
                <h1>Kochen, essen und kennenlernen!</h1>
                <!-- Timer -->
                <div id="clock"></div>   

                <!-- Newsletter -->
                <div id="newsletter">
                    <p>Wir sind ab dem 28. März 2015 für euch erreichbar. Hinterlasse deine E-Mail Adresse um von uns exklusiv informiert zu werden!</p>
                    <p>Ich will bei der <a target="_blank" href="http://beta.socialmeal.ch/Blog/social-meal-beginnt-mit-offener-beta-testphase-hilf-mit.html">Beta-Testphase</a> mitmachen!

                    <form method="post" action="contact.php" name="contactform" id="contactform">
                        <fieldset>
                            <input name="email" type="email" required="required" id="email" placeholder="Bitte deine E-Mail Adresse eingeben" />
                            <input type="submit" class="submit" id="submit" value="Informiert mich" />
                        </fieldset>
                    </form>

                    <div id="message"></div>

                </div>

                <!-- Social Links -->
                <div class="social">
                    <ul>
                        <li><a target="_blank" href="https://www.facebook.com/42socialmeal" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a target="_blank" href="https://twitter.com/infosocialmeal" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a target="_blank" href="https://plus.google.com/102044072731061267569" title="Google+"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Modal Window -->
        <div id="modal">
            <div class="container">
                <div class="one">
                    <h2>Social Meal - Meal-Sharing für die Region Basel</h2>
                    <a id="modal-close" href="#"><i class="fa fa-minus"> Zurück</i></a>
                </div>

                <div class="one-half">
                    <h3>Idee</h3>
                    <p>
                        Du kochst gerne? Du isst nicht gerne alleine? Du lernst gerne neue Leute kennen? Wir verbinden euch gerne! <a target="_blank" href="http://www.occupybasel.ch/unser-neues-projekt-social-meal-meal-sharing-fuer-die-region-basel/2282">Aktuelle Infos!</a>
                    </p>
                    
                    
                    <h3>Über uns</h3>
                    <p>
                        Dieses Projekt wird von Occupy Basel entwickelt und getragen. Wir wollen damit Gemeinschaften fördern, verschiedene Organisationen aus dem Nahrungsmittelbereich verknüpfen sowie einen Beitrag zu nachhaltigerer Ernährung leisten. Über Unterstützungs- und Mitwirkungsangebote jeglicher Art freuen wir uns sehr!
                    </p>
                    
                    <ul>
                        <li><i class="fa fa-home"></i><a href="http://occupybasel.ch" target="_blank">http://occupybasel.ch</a></li>
                    </ul>
                </div>
                
                

                <div class="one-half">
                    <h3>Unser Angebot</h3>
                    <p>
                        Registrierte Mitglieder erhalten die Möglichkeit, gemeinsame Ess- und Kochmöglichkeiten anzubieten oder in Anspruch zu nehmen. Des Weiteren bieten wir auch die Möglichkeit, gemeinsame Restaurantbesuche oder spezielle Events zu organisieren und zu bewerben. Ein Blog bietet Austauschmöglichkeiten über Erfahrungen sowie diverse Themen zur Ernährung.
                    </p>
                    <p>
	                    Ein Bewertungs- und Feedbacksystem sorgt für die Qualitätssicherung der Angebote. Verschlüsselte Datenübertragung (in der endgültigen Webseite) sowie strenge Richtlinien garantieren weitreichenden Datenschutz.
                    </p>
                    <ul>
                        <li><i class="fa fa-envelope"></i><a href="mailto:info@socialmeal.ch?subject=Kontaktanfrage">info@socialmeal.ch</a></li>
                    </ul>
                </div>
                
                <!-- Social Links -->
                <div class="one">
	                <div class="social">
	                    <ul>
	                        <li><a target="_blank" href="https://www.facebook.com/42socialmeal" title="Facebook"><i class="fa fa-facebook"></i></a></li>
	                        <li><a target="_blank" href="https://twitter.com/infosocialmeal" title="Twitter"><i class="fa fa-twitter"></i></a></li>
	                        <li><a target="_blank" href="https://plus.google.com/102044072731061267569" title="Google+"><i class="fa fa-google-plus"></i></a></li>
	                    </ul>
	                </div>
                </div>
                
                <p>&nbsp;<br />&nbsp;<br />&nbsp;<br />&nbsp;</p>
            </div>
            
            
        </div>

        <!-- Preload -->
        <div id="preload">
            <div id="preload-content">
                <img src="img/loaders.gif" alt="preload icon"/>
                <div class="preload-text">laden ...</div>
            </div>
        </div>

        <!-- Included JS Files -->
        <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="js/jquery.countdown.min.js"></script>
        <script type="text/javascript" src="js/custom.js"></script>
        <script type="text/javascript" src="js/plugins.js"></script>
    </body>
</html>